//
//  NewHighScoreScene.swift
//  StarFighter
//
//  Created by Ola on 25/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

class NewHighScoreScene : SKScene, UITextFieldDelegate{
    
    let backgroundImage = SKSpriteNode(imageNamed: "game_background")
    
     override init(size: CGSize) {
        super.init(size: size)
        
        backgroundImage.position = CGPoint(x: size.width / 2, y: size.height / 2)
        backgroundImage.zPosition = -1
        backgroundImage.size = self.frame.size
        addChild(backgroundImage)
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.fontSize = 30.0
        label.fontColor = SKColor.whiteColor()
        label.position = CGPoint(x: size.width / 2, y: size.height * 3/4)
        label.text = "NEW HIGH SCORE!"
        addChild(label)

    }
    
    
    
    override func didMoveToView(view: SKView) {
        
        let nickTextField = UITextField()
        nickTextField.placeholder = "Enter your name"
        nickTextField.frame = CGRectMake(size.width / 4, size.width / 2, size.width / 2, size.height / 10)
        nickTextField.textColor = UIColor.whiteColor()
        nickTextField.minimumFontSize = 20.0
        nickTextField.keyboardType = .Default
        nickTextField.backgroundColor = UIColor.lightGrayColor()
        nickTextField.borderStyle = .Line
        nickTextField.delegate = self
        self.view?.addSubview(nickTextField)
        
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        
        let currentCharacterCount = textField.text?.characters.count ?? 0
        if (range.length + range.location > currentCharacterCount){
            return false
        }
        let newLength = currentCharacterCount + string.characters.count - range.length
        return newLength <= 10
    }
  
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        print("RETURN!!!!!!!!!")
        if let name = textField.text where name.characters.count > 0 {
            print("TEXTFIELD.TEXT: |\(name)|")
            print("Dlugosc: \(name.characters.count)")
            textField.resignFirstResponder()
            textField.removeFromSuperview()
            GameState.sharedInstance.addNewScore(name: name)
            runAction(
                SKAction.runBlock(){
                    let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                    let scene = HighScoresScene(size: self.size)
                    self.view?.presentScene(scene, transition: reveal)
                }
            )

        }
        
        return true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
