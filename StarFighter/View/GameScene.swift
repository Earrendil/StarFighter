//
//  GameScene.swift
//  StarFighter
//
//  Created by Ola on 17/08/16.
//  Copyright (c) 2016 Aleksandra Wardak. All rights reserved.
//

import SpriteKit

struct PhysicsCategory {
    static let None : UInt32 = 0
    static let All : UInt32 = UInt32.max
    static let Meteor : UInt32 = 0b1
    static let Bonus : UInt32 = 0b10
    static let Projectile : UInt32 = 0b100
    
}

func + (left: CGPoint, right: CGPoint) -> CGPoint{
    return CGPoint(x: left.x + right.x, y: left.y + right.y)
}

func - (left: CGPoint, right: CGPoint) -> CGPoint{
    return CGPoint(x: left.x - right.x, y: left.y - right.y)
}

func * (point: CGPoint, scalar: CGFloat) -> CGPoint{
    return CGPoint (x: point.x * scalar, y: point.y * scalar)
}

func / (point: CGPoint, scalar: CGFloat) -> CGPoint{
    return CGPoint (x: point.x / scalar, y: point.y / scalar)
}

#if !(arch(x86_64) || arch(arm64))
    func sqrt(a: CGFloat) -> CGFloat{
        return CGFloat(sqrtf(Float(a)))
    }
#endif

extension CGPoint {
    func length() -> CGFloat{
        return sqrt( x * x + y * y )
    }
    
    func normalized() -> CGPoint {
        return self / length()
    }
}


class GameScene: SKScene, SKPhysicsContactDelegate{
    
    let player = Player(image: "Spaceship")
    let livesLabel = SKLabelNode(fontNamed: "Chulkduster")
    let scoresLabel = SKLabelNode(fontNamed: "Chulkduster")
    let backgroundImage = SKSpriteNode(imageNamed: "game_background")
    let earth = SKSpriteNode(imageNamed: "earth")
    var waitForBonus = 0
    var waitForMeteor = 1.3
    
    override init(size: CGSize) {
        super.init(size: size)
        GameState.sharedInstance.setBeginParams()
        Meteor.initVariables()
        waitForBonus = Int(arc4random_uniform(UInt32(5)) + 5)
        Projectile.upgraded = false
        
        runAction(SKAction.repeatActionForever(
            SKAction.sequence([
                SKAction.runBlock(addObject),
                SKAction.waitForDuration(waitForMeteor)
                ])
            ))
    }

    
    override func didMoveToView(view: SKView) {
        
        backgroundImage.position = CGPoint(x: size.width / 2, y: size.height / 2)
        backgroundImage.zPosition = -1
        backgroundImage.size = self.frame.size
        addChild(backgroundImage)
        
        earth.position = CGPoint(x: size.width / 2, y: earth.size.height / 4)
        earth.xScale = 1.5
        addChild(earth)
        
        player.setPosition(screenWidth: size.width, screenHeight: size.height)
        player.getNode().zPosition = 1
        addChild(player.getNode())
        

        livesLabel.text = String(GameState.sharedInstance.lives)
        livesLabel.position = CGPoint(x: size.width * 0.9, y: size.height * 0.9)
        livesLabel.fontSize = 26
        livesLabel.fontColor = SKColor.redColor()
        addChild(livesLabel)
        
        scoresLabel.text = String(GameState.sharedInstance.score)
        scoresLabel.position = CGPoint(x: size.width * 0.1, y: size.height * 0.9)
        scoresLabel.fontSize = 26
        scoresLabel.fontColor = SKColor.redColor()
        addChild(scoresLabel)
 
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        
    }
    
    func addObject(){
        print("waitForBonus: \(waitForBonus)")

        
        if (waitForBonus <= 0){
            print("Bonus appears")
            waitForBonus = Int(arc4random_uniform(UInt32(5)) + 5)
            addBonus()
        }
        else{
            print("Meteor appears")
            waitForBonus -= 1
            addMeteor()
        }
    }
    
    func addBonus() {
        let bonus = Bonus()
        bonus.setPosition(screenWidth: self.size.width, screenHeight: self.size.height)
        addChild(bonus.getNode())
        bonus.getNode().runAction(bonus.bonusAction())
    }
    
    func addMeteor() {
        
        let meteor = Meteor()
        meteor.setPosition(screenWidth: size.width, screenHeight: size.height)
        addChild(meteor.getNode())
        
        let loseAction = SKAction.runBlock(){
            GameState.sharedInstance.lives -= 1
            self.livesLabel.text = String(GameState.sharedInstance.lives)
            if (GameState.sharedInstance.lives <= 0) {
                let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                let gameOverScene = GameOverScene(size: self.size, won: false)
                self.view?.presentScene(gameOverScene, transition: reveal)
            }
           
        }

        meteor.getNode().runAction(SKAction.sequence([meteor.meteorAction(), loseAction]))
    }
    
    override func touchesEnded(touches: Set<UITouch>, withEvent event: UIEvent?) {

        runAction(SKAction.playSoundFileNamed("Grenade.wav", waitForCompletion: false))
        
        guard let touch = touches.first else {
            return
        }
 
        let touchLocation = touch.locationInNode(self)
        
        let projectile = Projectile(image: "projectile1", position: player.getPosition())
        
    
        let offset = touchLocation - projectile.node.position
        if (offset.y < 0) { return }
        
        addChild(projectile.node)
        projectile.projectileAction(offset)
    }
    
    func projectileDdidCollideWithMeteor (){
        player.setMeteorDestroyed(player.getMeteorDestroyed() + 1)
        if player.getMeteorDestroyed() > 500 {
            let reveal = SKTransition.flipHorizontalWithDuration(0.5)
            let gameOverScene = GameOverScene(size: self.size, won: true)
            self.view?.presentScene(gameOverScene, transition: reveal)
        }
        print("Hit!")
        
    }
    
    func didBeginContact(contact: SKPhysicsContact) {
        var firstBody : SKPhysicsBody
        var secondBody : SKPhysicsBody
        
        if contact.bodyA.categoryBitMask < contact.bodyB.categoryBitMask {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        } else {
            firstBody = contact.bodyB
            secondBody = contact.bodyA
        }
        
        if ((firstBody.categoryBitMask & PhysicsCategory.Bonus != 0) && (secondBody.categoryBitMask & PhysicsCategory.Projectile != 0)){
            if firstBody.node != nil && secondBody.node != nil{
                let bon = firstBody.node as! SKSpriteNode
                
                if let id = bon.name {
                    switch id {
                    case "bigProjectileBonus":
                        print("bon: biggerProjctiles")
                        Projectile.upgraded = true
                    case "smallProjectileBonus":
                        print("bon: smallerProjctiles")
                        Projectile.upgraded = false
                    case "extraLife":
                        print("bon: extraLife")
                        GameState.sharedInstance.lives += 1
                        livesLabel.text = String(GameState.sharedInstance.lives)
                    default:
                        print("default")
                    }
                }

                
                
                if (firstBody.node != nil) {
                    firstBody.node!.removeFromParent()
                    
                }
                else {
                    print("zlikwidowano blad!!!")
                }
                
            }
            if (secondBody.node != nil) {
                secondBody.node!.removeFromParent()
            }
        }
    
        if ((firstBody.categoryBitMask & PhysicsCategory.Meteor != 0) && (secondBody.categoryBitMask & PhysicsCategory.Projectile != 0)){
            if firstBody.node != nil && secondBody.node != nil{
                let met = firstBody.node as! SKSpriteNode
                let proj = secondBody.node as! SKSpriteNode
                print("Xscale: \(proj.xScale)")
                if Projectile.upgraded {
                    met.colorBlendFactor += 0.4
                    print("BLEND +0.4")
                    GameState.sharedInstance.score += 10
                    scoresLabel.text = String(GameState.sharedInstance.score)
                } else {
                    met.colorBlendFactor += 0.2
                    print("BLEND +0.2")
                    GameState.sharedInstance.score += 5
                    scoresLabel.text = String(GameState.sharedInstance.score)
                }
                
                if (met.colorBlendFactor >= 0.5) {
                    if (firstBody.node != nil) {
                        firstBody.node!.removeFromParent()
                    }
                    else {
                        print("zlikwidowano blad!!!")
                    }
                }
            }
            
            if (secondBody.node != nil) {
                 secondBody.node!.removeFromParent()
                projectileDdidCollideWithMeteor()
            }
                    
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
