//
//  GameOverScene.swift
//  Gra
//
//  Created by Ola on 19/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import SpriteKit

class GameOverScene : SKScene {
    
    init (size: CGSize, won: Bool){
        super.init(size: size)
        
        backgroundColor = SKColor.whiteColor()
        let message = won ? "YOU WON!" : "GAME OVER!"
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.text = message
        label.fontSize = 40
        label.fontColor = SKColor.blackColor()
        label.position = CGPoint(x: size.width / 2, y: size.height / 2  + label.frame.size.height)
        addChild(label)
        
        let points = SKLabelNode(fontNamed: "Chalkduster")
        points.text = "Score: \(GameState.sharedInstance.score)"
        points.fontSize = 40
        points.fontColor = SKColor.blackColor()
        points.position = CGPoint(x: size.width / 2, y: size.height / 2 - points.frame.size.height)
        addChild(points)
        
        }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        print("wynik: \(GameState.sharedInstance.score)")
        print("najlpeszy wynik: \(GameState.sharedInstance.highScoreArray[2].score)")
        
        if(GameState.sharedInstance.score > GameState.sharedInstance.highScoreArray[2].score){
            runAction(
                SKAction.runBlock(){
                    let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                    let scene = NewHighScoreScene(size: self.size)
                    self.view?.presentScene(scene, transition: reveal)
                }
            )
        } else {
        runAction(
            SKAction.runBlock(){
                let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                let scene = MainMenuScene(size: self.size)
                self.view?.presentScene(scene, transition: reveal)
            }
        )
        }
    }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
