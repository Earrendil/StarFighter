//
//  HighScoresScene.swift
//  StarFighter
//
//  Created by Ola on 25/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

class HighScoresScene :SKScene {

    let backgroundImage = SKSpriteNode(imageNamed: "game_background")
    
    override init(size: CGSize) {
        super.init(size: size)
        
        backgroundImage.position = CGPoint(x: size.width / 2, y: size.height / 2)
        backgroundImage.zPosition = -1
        backgroundImage.size = self.frame.size
        addChild(backgroundImage)
        
        
        addScore(String(format: "1. \(GameState.sharedInstance.highScoreArray[0].name) %d", GameState.sharedInstance.highScoreArray[0].score), offset: 1.5)
        addScore(String(format: "2. \(GameState.sharedInstance.highScoreArray[1].name) %d", GameState.sharedInstance.highScoreArray[1].score), offset: 0.0)
        addScore(String(format: "3. \(GameState.sharedInstance.highScoreArray[2].name) %d", GameState.sharedInstance.highScoreArray[2].score), offset: -1.5)
    }
    
    func addScore(text: String, offset: CGFloat) {
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.fontColor = SKColor.whiteColor()
        label.fontSize = 30.0
        label.text = text
        label.position = CGPoint(x: size.width / 2, y: size.height / 2 + label.fontSize * offset)
        addChild(label)
    }
    
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
                runAction(
                    SKAction.runBlock(){
                        let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                        let scene = MainMenuScene(size: self.size)
                        self.view?.presentScene(scene, transition: reveal)
                    }
                )
                
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
