//
//  MainMenuScene.swift
//  StarFighter
//
//  Created by Ola on 25/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import SpriteKit

class MainMenuScene :SKScene {
    
    let backgroundImage = SKSpriteNode(imageNamed: "game_background")
    
    func addButton(title: String, offset: CGFloat){
        let button = SKSpriteNode()
        button.color = SKColor.lightGrayColor()
        button.size = CGSize(width: size.width, height: size.height / 6)
        button.position = CGPoint(x: size.width / 2, y: size.height / 2  + button.size.height * offset)
        button.name = title
        addChild(button)
        
        let label = SKLabelNode(fontNamed: "Chalkduster")
        label.fontColor = SKColor.blackColor()
        label.fontSize = 40.0
        label.text = title
        label.name = title
        label.position = button.position
        addChild(label)

    }
    
    override init(size: CGSize) {
        super.init(size: size)
        
        backgroundImage.position = CGPoint(x: size.width / 2, y: size.height / 2)
        backgroundImage.zPosition = -1
        backgroundImage.size = self.frame.size
        addChild(backgroundImage)

        addButton("NEW GAME", offset: 1.1 )
        addButton("HIGH SCORES", offset: 0.0)
        addButton("QUIT", offset:  -1.1)
       
            }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        let touch = touches.first
        let location = touch?.locationInNode(self)
        let node = nodeAtPoint(location!)
        
        if let id = node.name {
            switch id {
            case "NEW GAME":
                print("newGameButton")
                runAction(
                    SKAction.runBlock(){
                        let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                        let scene = GameScene(size: self.size)
                        self.view?.presentScene(scene, transition: reveal)
                    }
                )

            case "HIGH SCORES":
                print  ("High scores")
                runAction(
                    SKAction.runBlock(){
                        let reveal = SKTransition.flipHorizontalWithDuration(0.5)
                        let scene = HighScoresScene(size: self.size)
                        self.view?.presentScene(scene, transition: reveal)
                    }
                )

            case "QUIT":
                print("Quit")
                exit(0)
            default:
                print("default")
            }
        }
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
