//
//  Projectile.swift
//  StarFighter
//
//  Created by Ola on 24/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class Projectile {
    
    let node: SKSpriteNode
    
   static var upgraded = false
    
    init (image: String, position: CGPoint){
        node = SKSpriteNode(imageNamed: image)
        node.position = position
        
        if Projectile.upgraded {
            node.setScale(2.0)
        }
        
        
        node.physicsBody = SKPhysicsBody(rectangleOfSize: node.size)
        node.physicsBody?.dynamic = true
        node.physicsBody?.categoryBitMask = PhysicsCategory.Projectile
        node.physicsBody?.contactTestBitMask = PhysicsCategory.Meteor
        node.physicsBody?.collisionBitMask = PhysicsCategory.None
        node.physicsBody?.usesPreciseCollisionDetection = true

    }
    
    func setPosition(screenWidth screenWidth: CGFloat, screenHeight: CGFloat) {
        node.position = CGPoint(x: screenWidth * 0.5, y: screenHeight * 0.1)
    }
    
    
    func getPosition() -> CGPoint{
        return CGPoint(x: node.position.x, y: node.position.y)
    }
    
    func projectileAction (offset: CGPoint) {
        let direction = offset.normalized()
        let shootAmount = direction * 1000
        let realDest = shootAmount + node.position
        let actionMove = SKAction.moveTo(realDest, duration: 2.0)
        let actionMoveDone = SKAction.removeFromParent()
        node.runAction(SKAction.sequence([actionMove, actionMoveDone]))
    }
}
