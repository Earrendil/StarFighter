//
//  Meteor.swift
//  StarFighter
//
//  Created by Ola on 24/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

class Meteor {
    private let node: SKSpriteNode
    
    struct meteorKind {
        var image : String
        var initColorBlendFactor : CGFloat
    }

    
   private class func randomMeteor() -> meteorKind{
        var meteorImages = [meteorKind]()
        meteorImages.append(meteorKind(image: "meteor4", initColorBlendFactor: 0.4))
        meteorImages.append(meteorKind(image: "meteor2", initColorBlendFactor: 0.2))
        meteorImages.append(meteorKind(image: "meteor3", initColorBlendFactor: 0.0))
        let number = Int(arc4random_uniform(UInt32(meteorImages.count)))
        return meteorImages[number]
    }
    
    init (){
        let met = Meteor.randomMeteor()
        node = SKSpriteNode(imageNamed: met.image)
        node.colorBlendFactor = met.initColorBlendFactor
        
        node.physicsBody =  SKPhysicsBody(rectangleOfSize: node.size)
        node.physicsBody?.dynamic = true
        node.physicsBody?.categoryBitMask = PhysicsCategory.Meteor
        node.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile
        node.physicsBody?.collisionBitMask = PhysicsCategory.None
    }
    
    func getNode() -> SKSpriteNode{
        return node
    }
    
    func setPosition(screenWidth screenWidth: CGFloat, screenHeight: CGFloat) {
        let actualX = random(min: node.size.width / 2, max: screenWidth - node.size.width / 2)
        node.position = CGPoint(x: actualX, y: screenHeight + node.size.height / 2)
    }
    
    func getPosition() -> CGPoint{
        return CGPoint(x: node.position.x, y: node.position.y)
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min min: CGFloat, max: CGFloat) ->  CGFloat {
        if max < min { return min }
        return random() * (max - min) + min
    }
    
    static var a = 5.0
    static var b = 7.0
    
   static func initVariables() {
        Meteor.a = 5.0
        Meteor.b = 7.0
    }
    
    func meteorAction() -> SKAction {
        
        let actualSpeed = random(min: CGFloat(Meteor.a), max: CGFloat(Meteor.b))
        if ( Meteor.a > 2.0){
            Meteor.a -= 0.1
            Meteor.b -= 0.1
        }
        
        return SKAction.moveTo (CGPoint(x: node.position.x, y: -node.size.height / 2), duration: NSTimeInterval(actualSpeed))
        
    }
}
    

    
    



