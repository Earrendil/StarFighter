//
//  Player.swift
//  StarFighter
//
//  Created by Ola on 24/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

class Player {
    
    private let node: SKSpriteNode
    
    private var meteorDestroyed = 0
    
    init (image: String){
        node = SKSpriteNode(imageNamed: image)
    }
    
    func getNode() -> SKSpriteNode{
        return node
    }
    
    func setMeteorDestroyed(value: Int) {
        meteorDestroyed = value
    }
    
    func getMeteorDestroyed() -> Int {
        return meteorDestroyed
    }
    
    func setPosition(screenWidth screenWidth: CGFloat, screenHeight: CGFloat) {
        node.position = CGPoint(x: screenWidth * 0.5, y: screenHeight * 0.1)
    }
    
    
    func getPosition() -> CGPoint{
        return CGPoint(x: node.position.x, y: node.position.y)
    }
    
    
}
