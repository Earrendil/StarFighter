//
//  Score.swift
//  StarFighter
//
//  Created by Ola on 29/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation

class Score : NSObject {
    
    var name: String
    var score: Int
    
    init(name: String, score: Int){
        self.name = name
        self.score = score
    }
    
    init(coder decoder: NSCoder) {
        self.name = decoder.decodeObjectForKey("name") as! String
        self.score = decoder.decodeIntegerForKey("score")
    }
    
    func encodeWithCoder(coder: NSCoder) {
        coder.encodeObject(self.name, forKey: "name")
        coder.encodeInt(Int32(self.score), forKey: "score")
    }
}

