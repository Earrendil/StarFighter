//
//  GameState.swift
//  StarFighter
//
//  Created by Ola on 28/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation


public struct HighScoreStruct {
    var name : String?
    var score : Int?
    
    public init(name: String, score: Int){
        self.name = name
        self.score = score
    }
    
    public init(dictionary: Dictionary<String, AnyObject>){
        name = dictionary["name"] as? String
        score = dictionary["score"] as? Int
    }

    public func encode() -> Dictionary<String, AnyObject>{
        var dictionary = Dictionary<String, AnyObject>()
        dictionary["name"] = name
        dictionary["score"] = score
        return dictionary
    }
    
}

class GameState {
    
    var score: Int
    var highScoreArray : [Score]
    var lives : Int
    
    class var sharedInstance: GameState {
        struct Singleton {
            static let instance = GameState()
        }
        return Singleton.instance
    }
    
    init(){
        score = 0
        lives = 3
        highScoreArray = [Score]()
    
        highScoreArray.append(Score(name: "nobody0", score: 0))
        highScoreArray.append(Score(name: "nobody1", score: 0))
        highScoreArray.append(Score(name: "nobody2", score: 0))
        let defaults = NSUserDefaults.standardUserDefaults()
        
        if let data = defaults.objectForKey("highScoreArray") as? NSData {
            highScoreArray = NSKeyedUnarchiver.unarchiveObjectWithData(data) as! [Score]
        }
        //------------------------------
       // resetDefaults()
        print("init")
        print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
        print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
        print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)

    }
    
    func saveState(){
        
        let defaults = NSUserDefaults.standardUserDefaults()
        
        let data = NSKeyedArchiver.archivedDataWithRootObject(highScoreArray)
        defaults.setObject(data, forKey: "highScoreArray")
    
        NSUserDefaults.standardUserDefaults().synchronize()
        
        print("saveState")
        print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
        print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
        print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)
    }
    
    func addNewScore(name name: String){
        if (score > highScoreArray[1].score) {
            print("---> 1")
          //  swap(&highScoreArray[2], &highScoreArray[1])
            highScoreArray[2].score = highScoreArray[1].score
            highScoreArray[2].name = highScoreArray[1].name
            
            print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
            print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
            print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)
            if (score > highScoreArray[0].score) {
                print("---> 2")
                highScoreArray[1].score = highScoreArray[0].score
                highScoreArray[1].name = highScoreArray[0].name
                
                print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
                print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
                print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)
                print("---> 2a")
                highScoreArray[0].score = score
                highScoreArray[0].name = name
                
                print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
                print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
                print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)
            } else {
                highScoreArray[1].score = score
                highScoreArray[1].name = name
                print("---> 3")
                print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
                print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
                print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)
            }
        }else {
            highScoreArray[2].score = score
            highScoreArray[2].name = name
            print("---> 4")
            print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
            print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
            print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)
        }
        print("addNewScore")
        print("0. \(highScoreArray[0].name) ", highScoreArray[0].score)
        print("1. \(highScoreArray[1].name) ", highScoreArray[1].score)
        print("2. \(highScoreArray[2].name) ", highScoreArray[2].score)
        
        saveState()
    }
    
    func resetDefaults(){
        highScoreArray[0] = Score(name: "nobody", score: 0)
        highScoreArray[1] = Score(name: "nobody", score: 0)
        highScoreArray[2] = Score(name: "nobody", score: 0)
        saveState()
    }
    
    func setBeginParams(){
        lives = 3
        score = 0
    }
    
}