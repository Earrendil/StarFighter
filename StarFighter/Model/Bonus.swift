//
//  Bonus.swift
//  StarFighter
//
//  Created by Ola on 25/08/16.
//  Copyright © 2016 Aleksandra Wardak. All rights reserved.
//

import Foundation
import SpriteKit
import UIKit

class Bonus {
    
    private let node: SKSpriteNode
    
   static var bonusArray = [String](arrayLiteral: "bigProjectileBonus", "smallProjectileBonus", "extraLife")
    
    private class func randomBonus() -> String{
        
        let number = Int(arc4random_uniform(UInt32(bonusArray.count)))
        return bonusArray[number]
    }
    
    init (){
        let bonImage = Bonus.randomBonus()
        node = SKSpriteNode(imageNamed: bonImage)
        node.name = bonImage
        
        node.physicsBody =  SKPhysicsBody(rectangleOfSize: node.size)
        node.physicsBody?.dynamic = true
        node.physicsBody?.categoryBitMask = PhysicsCategory.Bonus
        node.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile
        node.physicsBody?.collisionBitMask = PhysicsCategory.None
    }
 
    func getNode() -> SKSpriteNode{
        return node
    }
    
    func random() -> CGFloat {
        return CGFloat(Float(arc4random()) / 0xFFFFFFFF)
    }
    
    func random(min min: CGFloat, max: CGFloat) ->  CGFloat {
        if max < min { return min }
        return random() * (max - min) + min
    }
    
    func setPosition(screenWidth screenWidth: CGFloat, screenHeight: CGFloat) {
        let actualX = random(min: node.size.width / 2, max: screenWidth - node.size.width / 2)
        node.position = CGPoint(x: actualX, y: screenHeight + node.size.height / 2)
    }

    func bonusAction() -> SKAction {
        print("bonus Action")
        let actualSpeed = random(min: CGFloat(3.0), max: CGFloat(4.0))
        return SKAction.moveTo (CGPoint(x: node.position.x, y: -node.size.height / 2), duration: NSTimeInterval(actualSpeed))
        
    }
    
    func waitForBonus() -> Int {
        return Int(arc4random_uniform(UInt32(1)))
    }

}