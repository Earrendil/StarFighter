//
//  GameViewController.swift
//  Gra
//
//  Created by Ola on 17/08/16.
//  Copyright (c) 2016 Aleksandra Wardak. All rights reserved.
//

import UIKit
import SpriteKit

class GameViewController: UIViewController {
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let scene = MainMenuScene(size: view.bounds.size)
        let skView = self.view as! SKView
        skView.showsFPS = true
        skView.showsNodeCount = true
        
        skView.ignoresSiblingOrder = true
        
        scene.scaleMode = .ResizeFill
        
        skView.presentScene(scene)
        
        
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
